const full = location.protocol + '//' + location.host;
let baseAPI = full+'/api/webPush';

let appgmApiKey = 'keywebpush';

let request = axios.create({
  headers: {
    "appgm-key" : appgmApiKey
  }
});

const suscribirse = () => {

    request.post(baseAPI + '/getKeyPublic')
    .then(function(resp){

        const KEY_PUBLIC = resp.data.key;

        const subscription = async () => {
            
            const register = await navigator.serviceWorker.register("/js/worker.js");

            const subscription = await register.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: urlBase64ToUint8Array(KEY_PUBLIC)
            });

            // SUSCRIBIRSE  
            request.post(baseAPI + '/subscription', {
                subscription: subscription,
                idUser: 'demo',
                deviceType: 'Desktop',
                nameUser: 'Fither',
                browser: 'Google Chrome'
            }).then(function(){
                alert("Notificación activada y Suscrito correctamente!")
            });        

        };

        function urlBase64ToUint8Array(base64String) {
            const padding = "=".repeat((4 - (base64String.length % 4)) % 4);
            const base64 = (base64String + padding).replace(/-/g, "+").replace(/_/g, "/");
            const rawData = window.atob(base64);
            const outputArray = new Uint8Array(rawData.length);
            for (let i = 0; i < rawData.length; ++i) {
                outputArray[i] = rawData.charCodeAt(i);
            }
            return outputArray;
        }

        if ("serviceWorker" in navigator) {
            subscription().catch(err => console.log(err));
        }

    });

}

window.onload = () => {
    suscribirse();

    document.querySelector('form').addEventListener('submit', (e) => {

        e.preventDefault();

        // ENVIAR NOTIFICACIÓN  
        request.post(baseAPI + '/sendNotification', {
            arrayIdUsers: ['demo'],
            notification:{
                title: document.querySelector('#titulo').value,
                message: document.querySelector('#mensaje').value,
                icon: document.querySelector('#icono').value,
                image: document.querySelector('#imagen').value,
                url: document.querySelector('#url').value,
            }
        }).then(function(){
            alert("Enviado!")
        });   

    });

    document.querySelector('#activarNotificaciones').addEventListener('click', function(){

        // Comprobamos si el navegador soporta las notificaciones
        if (!("Notification" in window)) {
            alert("Este navegador no es compatible con las notificaciones de escritorio");
        }
        else if (Notification.permission === "granted") {
            suscribirse();
            // Si es correcto, lanzamos una notificación
        }else if (Notification.permission !== "denied") {
            suscribirse();
        }

    });

}