# Notificaciones WebPush (Cliente)

Para poder hacer uso de los servicios necesitas el `APIKEY de AppGm`, solicítalo [aquí](https://api.whatsapp.com/send?phone=51927468579), es gratuito al día de hoy.

También puedes comunicarte por medio de [facebook.com/appgm.link](https://www.facebook.com/appgm.link)

Nota: Este servicio es gratuito para las primeras integraciones que soliciten su ApiKey, las funciones del servicio la día de hoy contienen los siguientes datos: Título, mensaje, ícono, imagen y url de redirección.

Las comunicaciones del API deberán implementarlos del lado del servidor para evitar exponer la APIKEY y evitar fraude.
